# -*- coding: utf-8 -*-
"""
#-------------------------------* Description *--------------------------------#
                                     Empty.
                                     
#----------------------------* Script Infomation *-----------------------------#
                    File Name       : main.py
                    From Project    : qianzhanyan_spider
                    Created Date    : 2021-01-15 上午9:01
                    Author          : yuzaihhhh
                    Write With      : PyCharm
"""
__author__ = "yuzaihhhh"

import asyncio
import pandas as pd

from pipeline import IncomeStatement
from spider import QianzhanSpider

headers = {
    "Accept-Encoding": "gzip",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0",
    "Cookie": "qznewsite.uid=5yu3j445zqybtk553lr4msj2; qz.newsite=5A9668D076F09378748810E3D15B0DB0680BF0B47AB37314CFD513657F91B0CF9432B982AA038145EFA14945B4CF5AF5F9AA1FAF51191AAD69F5877C0E546AD0DF11B68005CC20DFF875DD2442E6EA458272A1ACC5091E1304E8DC1E9DA70444A0FB87EC67D119F5E487683D94FDC50704C2441A8418DA228B3984638CC8EC3DC55E0446048BA940D5DB1765C48364FD2DE2742C; user.email=15574796577"
}

if __name__ == '__main__':

    excel_file_name = '2011-2019计算机、通讯与其他制造业利润年报' + ".xlsx"
    query_options = {
        "nian": "2000",
        "jidu": "年报",
        "zoom": "1",
        "trade": "C39",
        "sort": "",
        "where": "",
        "page": "1"
    }

    async def spider_task(query_options, excel_file_name):

        async with QianzhanSpider(headers=headers) as spider:
            _counts = 1
            for _year in range(2011, 2020):
                query_options['nian'] = _year
                spider.set_query_options(query_options)
                await spider.parse()
                raw_data = await spider.extract()
                try:
                    with pd.ExcelWriter(excel_file_name, mode='a') as writer:
                        data_frame = IncomeStatement(data=raw_data)
                        data_frame.to_excel(writer, sheet_name='%s年' % _year, index=False)
                except FileNotFoundError:
                    with pd.ExcelWriter(excel_file_name) as writer:
                        data_frame = IncomeStatement(data=raw_data)
                        data_frame.to_excel(writer, sheet_name='%s年' % _year, index=False)
                except ValueError:
                    pass
                print(2)

    asyncio.run(spider_task(query_options, excel_file_name))
