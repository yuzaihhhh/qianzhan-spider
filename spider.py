# -*- coding: utf-8 -*-
"""
#-------------------------------* Description *--------------------------------#
                                     Empty.
                                     
#----------------------------* Script Infomation *-----------------------------#
                    File Name       : spider.py
                    From Project    : qianzhanyan_spider
                    Created Date    : 2021-01-15 上午11:42
                    Author          : yuzaihhhh
                    Write With      : PyCharm
"""
__author__ = "yuzaihhhh"

import lxml.html as lhtml

from abc import ABCMeta, abstractmethod
from httpx import AsyncClient
from httpx._utils import get_logger

logger = get_logger('HTTP Requests')


class Spider(AsyncClient, metaclass=ABCMeta):

    def __init__(self, *args, **kwargs):
        super(Spider, self).__init__(*args, **kwargs)
        self._result = None

    @abstractmethod
    async def parse(self):
        pass

    async def extract(self):
        return self._result


class QianzhanSpider(Spider):

    def __init__(self, *args, **kwargs):
        super(QianzhanSpider, self).__init__(*args, **kwargs)
        self._query_options = {}

    def set_query_options(self, qo: dict):
        self._query_options = qo

    async def parse(self):
        query_options = self._query_options
        raw_data = []

        for page in range(1, 2 ** 128):
            query_options["page"] = '%s' % page
            response = await self.post("https://stock.qianzhan.com/report/result/hs_lirun", data=query_options)
            logger.info('Request `https://stock.qianzhan.com/report/result/hs_lirun` data: {{query_options}}'.format(
                query_options
            ))
            print('Request `https://stock.qianzhan.com/report/result/hs_lirun` data: {query_options}'.format(
                query_options=query_options
            ))

            html_element = lhtml.fromstring(response.text)
            table_element = html_element.xpath("//table[@id='tblBody1']/tbody")[0]
            row_list = table_element.findall('.//tr')

            if not row_list: break

            dash_list = [dash.findall('.//td') for dash in row_list]
            raw_data.extend([(item.text_content() for item in dash) for dash in dash_list])
            """
            raw_data2 = [
                [[item.text_content()
                  for item in _dash]
                 for _dash in dash.findall('.//td')]
                for dash in row_list]
            """

        self._result = raw_data
        self._result_check = id(self._result)
