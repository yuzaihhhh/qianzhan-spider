# -*- coding: utf-8 -*-
"""
#-------------------------------* Description *--------------------------------#
                                     Empty.
                                     
#----------------------------* Script Infomation *-----------------------------#
                    File Name       : pipeline.py
                    From Project    : qianzhanyan_spider
                    Created Date    : 2021-01-15 上午11:38
                    Author          : yuzaihhhh
                    Write With      : PyCharm
"""
__author__ = "yuzaihhhh"


import pandas as pd


class IncomeStatement(pd.DataFrame):
    
    def __init__(self, **kwargs):
        kwargs['columns'] = kwargs.get('columns', [
            "序号", "证券代码", "证券名称", "营业总收入(元)", "营业总成本(元)",
            "营业收入(元)", "营业利润(元)", "利润总额(元)", "净利润(元)", "归属母公司股东的净利润(元)",
            "非经常性损益(元)", "归属母公司股东的净利润扣除非经常性损益(元)"
        ])
        super(IncomeStatement, self).__init__(**kwargs)
